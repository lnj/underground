-- SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
--
-- SPDX-License-Identifier: MIT

underground = {}

srcpath = core.get_modpath("underground") .. "/src/"

dofile(srcpath .. "api.lua")
dofile(srcpath .. "stones.lua")
dofile(srcpath .. "ores.lua")
dofile(srcpath .. "mtg_integration.lua")
