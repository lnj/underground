-- SPDX-FileCopyrightText: 2016 Linus Jahn <lnj@kaidan.im>
--
-- SPDX-License-Identifier: MIT

underground.registered_stones = {}
underground.registered_ores = {}

function underground.register_stone_with_ore(ore, stone)
	stone.groups = stone.groups or {cracky = 3}

	-- Set name, description, texture and groups
	-- name format: 'modNameStone:stoneName_with_oreName'
	-- i.e.: 'morestones:granite_with_iron'
	local fullname = stone.modname .. ":" .. stone.name .. "_with_" .. ore.name

	-- Use given description or the name and make the first letter uppercase
	-- description format: 'Stonename Orename Ore' -> 'Granite Iron Ore'
	ore.description = stone.description .. " " .. (ore.description or ore.name:gsub("^%l", string.upper)) .. " Ore"

	-- Take the stone texture and add the ore texture as overlay
	ore.tiles = {(stone.with_ore_texture or stone.tiles[1]) .. "^" .. ore.texture}

	-- Make the ore as cracky as the stone
	ore.groups = ore.groups or {}
	ore.groups.cracky = stone.groups.cracky + (ore.additional_stone_crackyness or 0)
	if ore.groups.cracky < 1 then
		ore.groups.cracky = 1
	end

	-- Use sounds from stone
	ore.sounds = stone.sounds or default.node_sound_stone_defaults()

	-- Clean up
	local ore_mapgen = ore.mapgen or {}
	ore.additional_stone_crackyness = nil
	ore.mapgen = nil
	ore.texture = nil
	ore.name = nil

	core.register_node(":" .. fullname, ore)

	-- Register ore generations
	for _, oredef in pairs(ore_mapgen) do
		-- Set the fitting stone name
		oredef.wherein = stone.modname .. ":" .. stone.name
		-- default to use scatter ores
		oredef.ore_type = oredef.ore_type or "scatter"
		-- Set ore itemstring
		oredef.ore = fullname

		core.register_ore(oredef)
	end
end

--[[
	Registers a stone definition for stone-ore-combinations, but don't register
	a new stone node
]]
function underground.register_stone_definition(def)
	local stone_def = {
		modname = core.get_current_modname(),
		tiles = {"unknown_node.png"},
		sounds = default.node_sound_stone_defaults(),
		groups = {cracky = 3}
	}

	-- Overwrite defaults with new specs
	for k,v in pairs(def) do
		stone_def[k] = v
	end

	-- register the stone with all existing ores
	for _, ore in pairs(underground.registered_ores) do
		-- The ore-def will be changed, so we copy it to not overwrite the
		-- original in underground.registered_ores
		register_stone_with_ore(table.copy(ore), stone_def)
	end

	table.insert(underground.registered_stones, stone_def)

	return stone_def
end

--[[
	Registers a new stone node and registers the stone def for other ores
]]
function underground.register_stone(def)
	local stone_def = underground.register_stone_definition(def)

	-- Create a clean node def
	local modname = stone_def.modname
	local node_def = table.copy(stone_def)
	local cobble = def.cobble
	local block = def.block
	local brick = def.brick

	node_def.modname = nil
	node_def.name = nil
	node_def.cobble = nil
	node_def.block = nil
	node_def.brick = nil

	-- generate itemstrings
	local fullname = modname .. ":" .. def.name
	local cobblename = fullname .. "_cobble"
	local blockname = fullname .. "_block"
	local brickname = fullname .. "_brick"

	if cobble then
		node_def.drop = node_def.drop or cobblename
	end

	-- Register the stone
	core.register_node(modname .. ":" .. def.name, node_def)

	-- Register the cobble variant
	if cobble then
		local cobble_def = table.copy(node_def)
		cobble_def.description = node_def.description .. " Cobblestone"

		for k,v in pairs(cobble) do
			cobble_def[k] = v
		end

		core.register_node(cobblename, cobble_def)
	end

	-- Register the block variant
	if block then
		local block_def = table.copy(node_def)
		block_def.description = node_def.description .. " Block"

		for k,v in pairs(block) do
			block_def[k] = v
		end

		core.register_node(blockname, block_def)
	end

	-- Register the brick variant
	if brick then
		local brick_def = table.copy(node_def)
		brick_def.description = node_def.description .. " Brick"

		for k,v in pairs(brick) do
			brick_def[k] = v
		end

		core.register_node(brickname, brick_def)
	end
end

--[[
	Registers a new ore in combination with all allowed stones.
]]
function underground.register_ore(def)
	local ore_def = {
		name = "ore",
		texture = "blank.png",
		groups = {},
		additional_stone_crackyness = 0,
		mapgen = {}
	}

	-- Overwrite defaults with new specs
	for k,v in pairs(def) do
		ore_def[k] = v
	end

	-- Register this ore in all different types of stone
	for _, stone in pairs(underground.registered_stones) do
		underground.register_stone_with_ore(table.copy(ore_def), stone)
	end

	table.insert(underground.registered_ores, def)
end
