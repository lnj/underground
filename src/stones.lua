-- SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
--
-- SPDX-License-Identifier: MIT

underground.register_stone({
	description = "Red Granite",
	name = "red_granite",
	tiles = {"underground_red_granite.png"},
	groups = {cracky = 2}
})

underground.register_stone({
	description = "Black Granite",
	name = "black_granite",
	tiles = {"underground_black_granite.png"},
	groups = {cracky = 2}
})

underground.register_stone({
	description = "Rhyolite",
	name = "rhyolite",
	tiles = {{name = "underground_rhyolite_global.png", align_style = "world", scale = 4}},
	with_ore_texture = "underground_rhyolite.png",
	groups = {cracky = 3},
	cobble = {
		tiles = {"underground_rhyolite_cobble.png"}
	},
	block = {
		tiles = {{name = "underground_rhyolite_block_global.png", align_style = "world", scale = 4}}
	},
	brick = {
		tiles = {{name = "underground_rhyolite_brick_global.png", align_style = "world", scale = 4}}
	}
})

if core.get_modpath("technic") then
	underground.register_stone_definition({
		modname = "technic",
		description = "Granite",
		name = "granite",
		tiles = {"technic_granite.png"},
		groups = {cracky = 2}
	})

	underground.register_stone_definition({
		modname = "technic",
		description = "Marble",
		name = "marble",
		tiles = {"technic_marble.png"},
		groups = {cracky = 3}
	})
end
