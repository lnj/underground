-- SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
--
-- SPDX-License-Identifier: MIT

--[[

Ores from Minetest Game

]]

underground.register_ore({
	name = "coal",
	texture = "default_mineral_coal.png",
	drop = "default:coal_lump",
	mapgen = {
		{
			clust_scarcity = 8 * 8 * 8,
			clust_num_ores = 9,
			clust_size     = 3,
			y_min          = 1025,
			y_max          = 31000
		},
		{
			clust_scarcity = 8 * 8 * 8,
			clust_num_ores = 8,
			clust_size     = 3,
			y_min          = -31000,
			y_max          = 64
		},
		{
			clust_scarcity = 24 * 24 * 24,
			clust_num_ores = 27,
			clust_size     = 6,
			y_min          = -31000,
			y_max          = 0
		}
	}
})

underground.register_ore({
	name = "iron",
	texture = "default_mineral_iron.png",
	drop = "default:iron_lump",
	additional_stone_crackyness = -1,
	mapgen = {
		{
			clust_scarcity = 9 * 9 * 9,
			clust_num_ores = 12,
			clust_size     = 3,
			y_min          = 1025,
			y_max          = 31000
		},
		{
			clust_scarcity = 7 * 7 * 7,
			clust_num_ores = 5,
			clust_size     = 3,
			y_min          = -31000,
			y_max          = 0
		},
		{
			clust_scarcity = 24 * 24 * 24,
			clust_num_ores = 27,
			clust_size     = 6,
			y_min          = -31000,
			y_max          = -64
		}
	}
})

underground.register_ore({
	name = "copper",
	texture = "default_mineral_copper.png",
	drop = 'default:copper_lump',
	additional_stone_crackyness = -1,
	mapgen = {
		{
			clust_scarcity = 9 * 9 * 9,
			clust_num_ores = 5,
			clust_size     = 3,
			y_min          = 1025,
			y_max          = 31000,
		},
		{
			clust_scarcity = 12 * 12 * 12,
			clust_num_ores = 4,
			clust_size     = 3,
			y_min          = -63,
			y_max          = -16,
		},
		{
			clust_scarcity = 9 * 9 * 9,
			clust_num_ores = 5,
			clust_size     = 3,
			y_min          = -31000,
			y_max          = -64,
		}
	}
})

underground.register_ore({
	name = "mese",
	texture = "default_mineral_mese.png",
	drop = "default:mese_crystal",
	additional_stone_crackyness = -2,
	mapgen = {
		{
			clust_scarcity = 14 * 14 * 14,
			clust_num_ores = 5,
			clust_size     = 3,
			y_min          = 1025,
			y_max          = 31000
		},
		{
			clust_scarcity = 18 * 18 * 18,
			clust_num_ores = 3,
			clust_size     = 2,
			y_min          = -255,
			y_max          = -64
		},
		{
			clust_scarcity = 14 * 14 * 14,
			clust_num_ores = 5,
			clust_size     = 3,
			y_min          = -31000,
			y_max          = -256
		}
	}
})

underground.register_ore({
	name = "gold",
	texture = "default_mineral_gold.png",
	drop = "default:gold_lump",
	additional_stone_crackyness = -1,
	mapgen = {
		{
			clust_scarcity = 13 * 13 * 13,
			clust_num_ores = 5,
			clust_size     = 3,
			y_min          = 1025,
			y_max          = 31000
		},
		{
			clust_scarcity = 15 * 15 * 15,
			clust_num_ores = 3,
			clust_size     = 2,
			y_min          = -255,
			y_max          = -64
		},
		{
			clust_scarcity = 13 * 13 * 13,
			clust_num_ores = 5,
			clust_size     = 3,
			y_min          = -31000,
			y_max          = -256
		}
	}
})

underground.register_ore({
	name = "diamond",
	texture = "default_mineral_diamond.png",
	drop = "default:diamond",
	additional_stone_crackyness = -2,
	mapgen = {
		{
			clust_scarcity = 15 * 15 * 15,
			clust_num_ores = 4,
			clust_size     = 3,
			y_min          = 1025,
			y_max          = 31000
		},
		{
			clust_scarcity = 17 * 17 * 17,
			clust_num_ores = 4,
			clust_size     = 3,
			y_min          = -255,
			y_max          = -128
		},
		{
			clust_scarcity = 15 * 15 * 15,
			clust_num_ores = 4,
			clust_size     = 3,
			y_min          = -31000,
			y_max          = -256
		}
	}
})

