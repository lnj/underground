-- SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
--
-- SPDX-License-Identifier: MIT

local coal_def = {
	name = "coal",
	texture = "default_mineral_coal.png",
	drop = "default:coal_lump",
	mapgen = {
		{
			clust_scarcity = 8 * 8 * 8,
			clust_num_ores = 8,
			clust_size     = 3,
			y_min          = -256,
			y_max          = 64
		},
		{
			clust_scarcity = 24 * 24 * 24,
			clust_num_ores = 27,
			clust_size     = 6,
			y_min          = -256,
			y_max          = 0
		}
	}
}

local iron_def = {
	name = "iron",
	texture = "default_mineral_iron.png",
	drop = "default:iron_lump",
	additional_stone_crackyness = -1,
	mapgen = {
		{
			clust_scarcity = 7 * 7 * 7,
			clust_num_ores = 5,
			clust_size     = 3,
			-- level where desert stone begins:
			y_min          = -256,
			y_max          = 0
		}
	}
}

local copper_def = {
	name = "copper",
	texture = "default_mineral_copper.png",
	drop = 'default:copper_lump',
	additional_stone_crackyness = -1,
	mapgen = {
		{
			clust_scarcity = 12 * 12 * 12,
			clust_num_ores = 4,
			clust_size     = 3,
			y_min          = -63,
			y_max          = -16,
		},
		{
			clust_scarcity = 9 * 9 * 9,
			clust_num_ores = 5,
			clust_size     = 3,
			y_min          = -256,
			y_max          = -64,
		}
	}
}

--
-- Stones
--

local stone_defs = {
	{
		name = "desert_stone",
		description = "Desert Stone",
		modname = "default",
		tiles = {"default_desert_stone.png"}
	},
	{
		name = "desert_sandstone",
		description = "Desert Sandstone",
		modname = "default",
		tiles = {"default_desert_sandstone.png"}
	},
	{
		name = "sandstone",
		description = "Sandstone",
		modname = "default",
		tiles = {"default_sandstone.png"}
	},
	{
		name = "silver_sandstone",
		description = "Silver Sandstone",
		modname = "default",
		tiles = {"default_silver_sandstone.png"}
	}
}

for _, stone in ipairs(stone_defs) do
	for _, ore in ipairs({coal_def, iron_def, copper_def}) do
		underground.register_stone_with_ore(table.copy(ore), table.copy(stone))
	end
end
