<!--
SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>

SPDX-License-Identifier: CC0-1.0
-->

# Minetest Mod Underground Biomes

Adds many new stone types and more interesting underground biomes.

This project is [REUSE](https://reuse.software/spec/)-compliant.

